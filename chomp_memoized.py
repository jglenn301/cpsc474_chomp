import sys

''' Finds winning moves for Chomp.  Memoized!
'''

def successors(p):
    ''' Finds all the possible positions reachable in one move from
        the given position.  Returns a list of (move, result) pairs,
        where each move is a (column, number-to-take) pair.  For example,
        if p is [4, 3, 2] then the move (1, 2) is represented by
        ((1, 2), [4, 1, 1]) on the returned list.

        p -- a list of nonnegative integers (for example [5, 5, 3, 0, 0, 0])
    '''
    # the list of possible resulting positions
    result = []
    for move_group in range(0, len(p)):
        # can move on any non-zero group
        group_size = p[move_group]
        for take in range(1, group_size + 1):
            # move_group = 1, take=3
            # succ = [5, 2, 2, 0, 0, 0]
            remaining = group_size - take
            succ = (p[:move_group]
                    + [remaining]
                    + [min(h, remaining) for h in p[move_group + 1:]])
            result.append(((move_group, take), succ))
            # [((1, 3), [5, 2, 2, 0, 0, 0]), ...]
    return result


def winning_move(p, memo):
    ''' Finds and returns a winning move for the given position, or
        None if there is no winning move.

        p -- an increasing list of positive integers
    '''
    # find the first move for which the resulting position is a losing position
    # (does not have a winning position)
    if sum(p) == 0:
        return ()
    if tuple(p) in memo:
        return memo[tuple(p)]

    moves = successors(p)
    winning = [move for (move, pos) in moves if winning_move(pos, memo) is None]
    if len(winning) == 0:
        memo[tuple(p)] = None
        return None
    else:
        memo[tuple(p)] = winning[0]
        return winning[0]

    
def usage():
    print("USAGE: %s squares-in-column..." % sys.argv[0])

    
if __name__ == "__main__":
    try:
        state = [int(x) for x in sys.argv[1:]]
        if len([x for x in state if x < 0]) > 0:
            raise ValueError("brownies in column must be nonnegative")
        memo = dict()
        print(winning_move([x for x in state if x > 0], memo))
    except:
        usage()
